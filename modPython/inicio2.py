#Crear un programa que pida al usuario una oracion y muestre por pantalla el número de veces que contiene cada vocal.

oracion= input("Ingrese una oración: ").lower()

print("Esta oración tiene", oracion.count("a"), "letras 'A'")
print("Esta oración tiene", oracion.count("e"), "letras 'E'")
print("Esta oración tiene", oracion.count("i"), "letras 'I'")
print("Esta oración tiene", oracion.count("o"), "letras 'O'")
print("Esta oración tiene", oracion.count("u"), "letras 'U'")

#Crea un programa con una tupla con los meses del año, pide números al usuario, si el numero esta entre 1 y la longitud máxima de la tupla, muestra el contenido de esa posición sino muestra un mensaje de error.
#El programa termina cuando el usuario introduce un cero.

