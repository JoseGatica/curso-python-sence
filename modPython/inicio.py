print("Hola Mundo!")


num = 2
if num > 3:
    print("El número ", num, " es mayor a 3")
else:
    print("El número ", num, " es menor a 3")


saludo = "Hola "
nombre = "Bruno"
version = 3

print(saludo, nombre)
print("Esto es Python ",version)



### Introducir un número por teclado y decir si es par o impar
num = int(input('Introduzca un numero entero: '))

if num % 2 == 0:
    print("Par")

else:
    print("Impar")